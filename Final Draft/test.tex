\documentclass[10pt]{article}

\include{preamble2}

\title{Pok\'emon GO, ``Gotta Catch 'em All''}
% \author{Team Kanto (Asghar, Clayton , Huixiang, Nail, Wenbo)}
\author{
    Barnes II, Clayton\\
    \texttt{clayleroy2@gmail.com}
    \and
    Dehghani, Asghar\\
    \texttt{adehgha@uw.edu}
    \and
    Hassairi, Nail \\
    \texttt{hassairi@uw.edu}
    \and
    Situ, Huixiang \\
    \texttt{gzsthx@qq.com}
    \and
    Zhu, Wenbo \\
    \texttt{wbzhu@uw.edu}
}\begin{document}
\maketitle

\abstract{
In this project, we will compare various strategies of pok\'emon hunting in the mobile game
of Pok\'emon GO. pok\'emon, being restless creatures, sometimes break out of captivity
and need to be re-captured. In this project, we will test various strategies of preventing
the pok\'emon breaking out of captivity after being caught. One such strategy is
using a Razz berry. Other possible strategies involve using various types of 
pokeballs -- Regular, Great, and Ultra. In this preliminary report, we will
describe the project, the collection of preliminary sample of data, and the power
analysis that will help us determine how much sample we will ultimately need
to collect for the final submission of the project.
}

\clearpage





\section*{Introduction}
Pok\'emon GO is a location based reality game developed by Niantic in which the
player interacts with real world through their smartphone. More specifically, the
player has a character whose game location corresponds to the players current real-world
location. The player is able to gather resources at ``pok\'estops,'' which in the
real-world correspond to monuments, parks, libraries, stores or other places of interest.
As the name of the game implies, one main aspect of the game involves the players 
Pok\'emon. To acquire Pok\'emon, the player must catch them. The
character will engage with ``wild'' pok\'emon as (s)he explores the world. 
Upon engaging a wild pok\'emon, one may attempt to catch it. (The player clicks on the pok\'emon
to engage to begin the catch phase of the game.) See figure 1 \ref{gameplay} for an illustration of the stages leading up to a pok\'emon capture.
To catch a pok\'emon the player throws balls, called pok\'eballs, which will absorb the pok\'emon upon contact. Sometimes the pok\'emon may break free of the pok\'eball, in which case the player may make another attempt. Other times the attempt will be successful and the pok\'emon will be captured. This is where our experiment begins, as we wish to study the factors which influence the success of a pok\'emon catch.
\begin{figure}
\center
\includegraphics[scale = .12]{IMG_6439.PNG}
\includegraphics[scale = .12]{IMG_6440.PNG}
\includegraphics[scale = .12]{IMG_6443.PNG}
\includegraphics[scale = .12]{IMG_6444.PNG}
\label{gameplay}
\caption{From left to right: While Clayton is home, his character ClayOak is on the hunt for pok\'emon,
two of which are in view. After selecting the Marill (the blue pok\'emon), the gameplay changes
to a catch phase. ClayOak threw a regular pok\'eball and captured the Marill! ClayOak has
never captured a Marill before, so it is included in his pok\'emon records, called a pok\'edex,
along with other relevant attributes of this specific pok\'emon.}
\end{figure}

\section*{Model and Controlled/Uncontrollable factors}
There are two controllable factors, and several uncontrollable ones, that influence catch success. The main purpose of this study is to isolate and explain the influence that
the controlled factors have on catching pok\'emon.

\begin{itemize}
\item \emph{Pok\'eball type:} There are four types of pok\'eballs which are used 
in catching pok\'emon: regular pokeballs, great balls, ultra balls, and master balls.

\item \emph{Treats:} These are treats which one can give to a pok\'emon
one is attempting to catch. There are two kinds of treats we will be testing: \emph{Razz Berries} and \emph{Nanab berries.}
\end{itemize}

The following are uncontrollable factors which may influence the ability to catch pok\'emon:

\begin{itemize}
\item \emph{CP:} The Combat Power (CP) of a pok\'emon.
\item \emph{Species:} The pok\'emon species.
\item \emph{Level:} The level of the pok\'emon.
\item \emph{Level (of player):} The level of the player.
\item \emph{Quality of throw:} The quality of the pok\'eball throw will influence the catch success as well. This is a hard factor to control, so we list it here.
\end{itemize}
Hence our treatments are given by the type of pok\'eball and the kind (if any) of treat.
We have wondered whether the fact that our outcome variable is distributed
Bernoulli (catch/no catch) could be an impediment to the application of the F distribution
in our power analysis (and later hypothesis testing) because the variance of a Bernoulli depends on the
underlying probability $p$, which may differ between
the treatment groups. Given this, proceeding under the assumption of equal 
variance may not be the best course of action.

We discuss two ways of computing power.\\

\emph{Method I:} We use the regular F statistic and
distribution theory, regardless of the fact that our variances are not identical.\\

\emph{Method II:} is to notice that under the null distribution of all treatments being 
equal will also imply the variances are equal. So we could estimate power by
using a bootstrap test at the fixed alternative, using 
the critical $\alpha$ level garnered from the $F$ statistic determined under the null.
We proceed to compare the power estimates for both these methods with an $\alpha$ level 
of $5\%.$

\section*{Project Context and Potential Issues}

\begin{figure}
\center
\includegraphics[scale = .5]{catchHist.pdf}
\label{catchHist}
\caption{In this histogram 0 represents not successfully catching the pok\'emon, i.e. the
pok\'emon breaking free from the pok\'eball, while 1 represents a successful capture.}
\end{figure}

For our power analysis we primarily need an estimate of the variance of our outcome.
We have not settled yet on what form the outcome would take. One possibility is that
it would be a Bernoulli random variable that would represent a single ``catch.''
A catch happens when a pok\'emon trainer throws pokeballs at a pokemon until this
pok\'emon is ``hit,'' at which point this pok\'emon will be absorbed into the pokeball.
This situation may result in the pok\'emon remaining in the ball ($catch = 1$)
or with the pok\'emon breaking out of the ball after capture ($catch = 0$). 
% Wondering a bit about this:
% Failure
% may be followed by more hunting and eventual successful capture ($catch = 1$).

\bigskip


The online community has done some work on determining relative catch rates.
These hypotheses have been generated by bots that have been subsequently
forbidden by The Pokemon Company and The Pokemon Company has taken legal
action against people who continued to use such bots.
The baseline catch rate depends on the type and level of the Pokemon being
hunted. Lets denote the baseline catch rate $BCR$. Then the razz berry catch
rate is hypothesized to be the following:

\begin{equation}
RCR = 1.5 BCR
\end{equation}

The great ball catch rate is:
\begin{equation}
GBCR = 1 - (1-BCR)^{1.5}
\end{equation}

The ultra ball catch rate is supposed to be:
\begin{equation}
UBCR = 1 - (1-BCR)^{2}
\end{equation}



This information is also summarized in the following table with sample values
for $BCR$:

% latex table generated in R 3.3.1 by xtable 1.8-2 package
% Sun Nov  5 20:42:38 2017
\begin{table}[ht]
\centering
\begin{tabular}{rrrrr}
  \hline
 & bcr & gbcr & ubcr & razzcr \\ 
  \hline
1 & 0.25 & 0.35 & 0.44 & 0.38 \\ 
  2 & 0.50 & 0.65 & 0.75 & 0.75 \\ 
  3 & 0.75 & 0.88 & 0.94 & 1.12 \\ 
   \hline
\end{tabular}
\end{table}


\subsection*{Method I: Parametric Power Analysis}
For our power analysis we primarily need an estimate of the variance of our outcome.
We have not settled yet on what form the outcome would take. One possibility is that
it would be a Bernoulli random variable that would represent a single ``catch''.
A catch happens when a Pokemon trainer throws pokeballs at a pokemon until this
Pokemon is ``hit'', at which point this Pokemon will be absorbed into the pokeball.
This situation may result in the Pokemon remaining in the ball ($catch = 1$)
or with the Pokemon breaking out of the ball after capture ($catch = 0$). Failure
may be followed by more hunting and eventual successful capture ($catch = 1$).

\bigskip

We have collected a total of 203 observations.
In this collection we have not randomized the razz berry use but rather assigned
it in an ad hoc way, our more experienced players who own more razz berries went
Pokemon hunting so in this way there would be a correlation between player level,
Pokemon level and the use of razz berry. Keeping in mind that this is just a power
calculation we will do a bit of hand-waving for now on this issue.

\bigskip

In practice, we have not collected equal samples for all treatment groups but the following
power analysis will assume that we would. We have demonstrated the ability to collect
200 samples and assuming that these would be equally distributed into four treatment 
groups, our $n=50$ and we will use this in the following power computation. The mean 
baseline catch rate in our pilot sample is $.7$. Based on the formulas above this would
imply a razz catch rate of $1$ ($\tau_r = .3$), ultra ball catch rate of $.91$ ($\tau_u = .21$), and
great ball catch rate of $.84$ ($\tau_g = .14$).  The variance of the catch rate in our
pilot data is $.207$. 
Under the alternative hypothesis, the F statistic for testing the equal means
hypothesis is distributed F with $m - 1 = 3$ (4 treatment groups) and $N-m = 199$ degrees of freedom
and non-centrality parameter (assuming 50 observations per treatment group, 
which does not quite add up to 199 but that is not of the first order
of importance here).

This all leads to the following $\lambda$:

\begin{equation}
\lambda = n \frac{\sum_{i=1} \tau_i^2}{\sigma^2} = 50 \frac{(.3^2+.14^2+.21^2)}{.207} = 45.54167 
\end{equation}


Plugging these numbers into the formula for power with an $\alpha$ level of $5\%$, we obtain that a sample of
$203$ observations would have the likelihood 99.99938\% of rejecting the null
hypothesis if the alternative we specified above is true. This does not 
mean that we are ready to present the results as of this moment, since
we have mostly collected data in one of the treatment groups only. However,
this indicates that if we collect a sample of similar size with equal
representation of all treatment groups we will have around 100\% chance
of rejecting the null conditional on the alternative hypothesis specified above
is true.

% \begin{knitrout}
% \definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
% \begin{alltt}
% \hlstd{data} \hlkwb{<-} \hlkwd{read.csv}\hlstd{(}\hlstr{"pokemon_pilot_data_updated.csv"}\hlstd{)}
% \hlkwd{print}\hlstd{(}\hlkwd{names}\hlstd{(data))}
% \end{alltt}
% \begin{verbatim}
% ##  [1] "player"       "encounter_ID" "pokemon_name" "CP"           "message_type"
% ##  [6] "catch"        "ball_type"    "razz_berry"   "level"        "X"
% \end{verbatim}
% \begin{alltt}
% \hlstd{catch_data} \hlkwb{<-} \hlstd{data}\hlopt{$}\hlstd{catch[}\hlopt{!}\hlkwd{is.na}\hlstd{(data}\hlopt{$}\hlstd{catch)]}
% \hlstd{N} \hlkwb{<-} \hlkwd{length}\hlstd{(catch_data)}
% \hlstd{m} \hlkwb{<-} \hlnum{4}
% \hlcom{#print(catch_data)}
% \hlcom{#print(data$razz_berry[!is.na(data$catch)])}
% \hlcom{#print(data$ball_type[!is.na(data$catch)])}

% \hlkwd{print}\hlstd{(}\hlkwd{var}\hlstd{(catch_data))}
% \end{alltt}
% \begin{verbatim}
% ## [1] 0.2071892
% \end{verbatim}
% \begin{alltt}
% \hlkwd{print}\hlstd{(}\hlkwd{mean}\hlstd{(catch_data))}
% \end{alltt}
% \begin{verbatim}
% ## [1] 0.7093596
% \end{verbatim}
% \begin{alltt}
% \hlstd{bcr} \hlkwb{<-} \hlkwd{mean}\hlstd{(catch_data)}
% \hlkwd{print}\hlstd{(}\hlkwd{sum}\hlstd{((bcr}\hlopt{-}\hlstd{catch_data)}\hlopt{^}\hlnum{2}\hlstd{))}
% \end{alltt}
% \begin{verbatim}
% ## [1] 41.85222
% \end{verbatim}
% \begin{alltt}
% \hlstd{rcr} \hlkwb{<-} \hlkwd{min}\hlstd{(}\hlnum{1.5}\hlopt{*}\hlstd{bcr,} \hlnum{1}\hlstd{)} \hlcom{# razz catch rate}
% \hlstd{gbcr} \hlkwb{<-} \hlkwd{min}\hlstd{(}\hlnum{1}\hlopt{-}\hlstd{(}\hlnum{1}\hlopt{-}\hlstd{bcr)}\hlopt{^}\hlnum{.5}\hlstd{,} \hlnum{1}\hlstd{)}
% \hlstd{ubcr} \hlkwb{<-} \hlkwd{min}\hlstd{(}\hlnum{1}\hlopt{-}\hlstd{(}\hlnum{1}\hlopt{-}\hlstd{bcr)}\hlopt{^}\hlnum{2}\hlstd{,} \hlnum{1}\hlstd{)}

% \hlkwd{print}\hlstd{(N)}
% \end{alltt}
% \begin{verbatim}
% ## [1] 203
% \end{verbatim}
% \begin{alltt}
% \hlstd{alpha} \hlkwb{<-} \hlnum{.05}
% \hlstd{lambda} \hlkwb{<-} \hlnum{50}\hlopt{*}\hlstd{((rcr}\hlopt{-}\hlstd{bcr)}\hlopt{^}\hlnum{2}\hlopt{+}\hlstd{(gbcr}\hlopt{-}\hlstd{bcr)}\hlopt{^}\hlnum{2}\hlopt{+}\hlstd{(ubcr}\hlopt{-}\hlstd{bcr)}\hlopt{^}\hlnum{2}\hlstd{)}\hlopt{/}\hlkwd{var}\hlstd{(catch_data)}
% \hlkwd{print}\hlstd{(lambda)}
% \end{alltt}
% \begin{verbatim}
% ## [1] 45.54167
% \end{verbatim}
% \begin{alltt}
% \hlstd{current_power} \hlkwb{<-} \hlnum{1} \hlopt{-} \hlkwd{pf}\hlstd{(}\hlkwd{qf}\hlstd{(}\hlnum{1}\hlopt{-}\hlstd{alpha, m} \hlopt{-} \hlnum{1}\hlstd{, N} \hlopt{-} \hlstd{m), m} \hlopt{-} \hlnum{1}\hlstd{, N} \hlopt{-} \hlstd{m,} \hlkwc{ncp} \hlstd{= lambda)}
% \hlkwd{print}\hlstd{(current_power)}
% \end{alltt}
% \begin{verbatim}
% ## [1] 0.9999805
% \end{verbatim}
% \end{kframe}
% \end{knitrout}

\subsection*{Method II: Empirical Power Analysis -- Nonparametric Bootstrap F Test}
% The previous section used distribution theory to compute power of our current sample and
% crucially operated under the assumption that variances are equal in the treatment and 
% control groups. However, our outcome is distributed Bernoulli and Bernoulli variances
% are functions of the underlying probability of Pokemon escaping the pokeball, so under
% the alternative hypothesis these variance would be unequal. This does not affect
% testing the null hypothesis, since under the null the underlying probabilities are
% the same but it does affect the power computation, because that computation involves
% the alternative hypothesis. Consequently, 
In this section we will perform a Monte Carlo 
exercise to determine power empirically and compare the results with our result above.
In light of what we described in the section above about different pokemon having
different baseline catch rate, we could actually influence our baseline catch rate
by ignoring some pokemon and focusing on others. Since the variance of the Bernoulli
is a function of the baseline catch rate, we can manipulated variance this way and
this affects power. Hence, in the following Monte Carlo simulation, we have run
the simulations with different baseline catch rates to capture not only the relationship
between sample size and power, but also between baseline catch rate and power.
As one can see in the Figure below, if we collect 75 or more observations per treatment
group, we will have 100\% power no matter the baseline catch rate. We can alternatively
focus on pokemon with higher baseline catch rate and this would bring us more power
at a lower sample size.

% \begin{knitrout}
% \definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}


% {\ttfamily\noindent\color{warningcolor}{\#\# Warning in anova.lm(lm(bootstrap\_outcome \textasciitilde{} treatment)): ANOVA F-tests on an essentially perfect fit are unreliable}}

% {\ttfamily\noindent\color{warningcolor}{\#\# Warning in anova.lm(lm(bootstrap\_outcome \textasciitilde{} treatment)): ANOVA F-tests on an essentially perfect fit are unreliable}}

% {\ttfamily\noindent\color{warningcolor}{\#\# Warning in anova.lm(lm(bootstrap\_outcome \textasciitilde{} treatment)): ANOVA F-tests on an essentially perfect fit are unreliable}}

% {\ttfamily\noindent\color{warningcolor}{\#\# Warning in anova.lm(lm(bootstrap\_outcome \textasciitilde{} treatment)): ANOVA F-tests on an essentially perfect fit are unreliable}}

% {\ttfamily\noindent\color{warningcolor}{\#\# Warning in anova.lm(lm(bootstrap\_outcome \textasciitilde{} treatment)): ANOVA F-tests on an essentially perfect fit are unreliable}}

% {\ttfamily\noindent\color{warningcolor}{\#\# Warning in anova.lm(lm(bootstrap\_outcome \textasciitilde{} treatment)): ANOVA F-tests on an essentially perfect fit are unreliable}}

% {\ttfamily\noindent\color{warningcolor}{\#\# Warning in anova.lm(lm(bootstrap\_outcome \textasciitilde{} treatment)): ANOVA F-tests on an essentially perfect fit are unreliable}}

% {\ttfamily\noindent\color{warningcolor}{\#\# Warning in anova.lm(lm(bootstrap\_outcome \textasciitilde{} treatment)): ANOVA F-tests on an essentially perfect fit are unreliable}}

% {\ttfamily\noindent\color{warningcolor}{\#\# Warning in anova.lm(lm(bootstrap\_outcome \textasciitilde{} treatment)): ANOVA F-tests on an essentially perfect fit are unreliable}}

% {\ttfamily\noindent\color{warningcolor}{\#\# Warning in anova.lm(lm(bootstrap\_outcome \textasciitilde{} treatment)): ANOVA F-tests on an essentially perfect fit are unreliable}}\end{kframe}
% \end{knitrout}

% \begin{knitrout}
% \definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
% \begin{alltt}
% \hlcom{#powers <- unlist(lapply(samples, single_sample_size_sim))}
% \hlkwd{print}\hlstd{(powers)}
% \end{alltt}
% \begin{verbatim}
% ##     bcr sample power
% ## 1  0.25     25   0.5
% ## 2   0.5     25   0.9
% ## 3  0.75     25   1.0
% ## 4  0.25     50   0.8
% ## 5   0.5     50   1.0
% ## 6  0.75     50   1.0
% ## 7  0.25     75   1.0
% ## 8   0.5     75   1.0
% ## 9  0.75     75   1.0
% ## 10 0.25    100   1.0
% ## 11  0.5    100   1.0
% ## 12 0.75    100   1.0
% ## 13 0.25    125   1.0
% ## 14  0.5    125   1.0
% ## 15 0.75    125   1.0
% \end{verbatim}
% \end{kframe}
% \end{knitrout}

\begin{center}
\begin{tabular}{lll}
 bcr & sample & power \\ \hline
0.25 &     25 &   0.5\\
 0.5 &     25 &   0.9\\
0.75 &     25 &   1.0\\
0.25 &     50 &   0.8\\
 0.5 &     50 &   1.0\\
0.75 &     50 &   1.0\\
0.25 &     75 &   1.0\\
 0.5 &     75 &   1.0\\
0.75 &     75 &   1.0\\
0.25 &    100 &   1.0\\
 0.5 &    100 &   1.0\\
0.75 &    100 &   1.0\\
0.25 &    125 &   1.0\\
 0.5 &    125 &   1.0\\
0.75 &    125 &   1.0\\
\end{tabular}
\end{center}

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}

{\centering \includegraphics[scale = .5]{figure/minimal-power-graph-1} 

}

\caption[Power, sample size and outcome variance (BCR)]{Power, sample size and outcome variance (BCR)}\label{fig:power-graph}
\end{figure}


\end{knitrout}



\end{document}
