\documentclass[10pt]{article}\usepackage[]{graphicx}\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%
\let\hlipl\hlkwb

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[export]{adjustbox}

\title{Pok\'emon GO, ``Gotta Catch 'em All''}
% \author{Team Kanto (Asghar, Clayton , Huixiang, Nail, Wenbo)}
\author{
    Barnes II, Clayton\\
    \texttt{clayleroy2@gmail.com}
    \and
    Dehghani, Asghar\\
    \texttt{adehgha@uw.edu}
    \and
    Hassairi, Nail \\
    \texttt{hassairi@uw.edu}
    \and
    Situ, Huixiang \\
    \texttt{gzsthx@qq.com}
    \and
    Zhu, Wenbo \\
    \texttt{wbzhu@uw.edu}
}
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}


\maketitle

\abstract{
In this project, we will compare various strategies of pok\'emon hunting in the mobile game
of Pok\'emon GO. pok\'emon, being restless creatures, sometimes break out of captivity
and need to be re-captured. In this project, we will test various strategies of preventing
the pok\'emon breaking out of captivity after being caught. One such strategy is
using a Razz berry. Other possible strategies involve using various types of 
pokeballs -- Regular, Great, and Ultra. In this preliminary report, we will
describe the project, the collection of preliminary sample of data, and the power
analysis that will help us determine how much sample we will ultimately need
to collect for the final submission of the project.
}

\clearpage

\section{Introduction}
Pok\'emon GO is a location based reality game developed by Niantic in which the
player interacts with real world through their smartphone. More specifically, the
player has a character whose game location corresponds to the players current real-world
location. The player is able to gather resources at ``pok\'estops,'' which in the
real-world correspond to monuments, parks, libraries, stores or other places of interest.
As the name of the game implies, one main aspect of the game involves the players 
Pok\'emon. To acquire Pok\'emon, the player must catch them. The
character will engage with ``wild'' pok\'emon as (s)he explores the world. 
Upon engaging a wild pok\'emon, one may attempt to catch it. (The player clicks on the pok\'emon
to engage to begin the catch phase of the game.) See figure 1 \ref{gameplay} for an illustration of the stages leading up to a pok\'emon capture.
To catch a pok\'emon the player throws balls, called pok\'eballs, which will absorb the pok\'emon upon contact. Sometimes the pok\'emon may break free of the pok\'eball, in which case the player may make another attempt. Other times the attempt will be successful and the pok\'emon will be captured. This is where our experiment begins, as we wish to study the factors which influence the success of a pok\'emon catch.
\begin{figure}
\center
\includegraphics[scale = .12]{IMG_6439.PNG}
\includegraphics[scale = .12]{IMG_6440.PNG}
\includegraphics[scale = .12]{IMG_6443.PNG}
\includegraphics[scale = .12]{IMG_6444.PNG}
\label{gameplay}
\caption{From left to right: While Clayton is home, his character ClayOak is on the hunt for pok\'emon,
two of which are in view. After selecting the Marill (the blue pok\'emon), the gameplay changes
to a catch phase. ClayOak threw a regular pok\'eball and captured the Marill! ClayOak has
never captured a Marill before, so it is included in his pok\'emon records, called a pok\'edex,
along with other relevant attributes of this specific pok\'emon.}
\end{figure}

\bigskip

There are two controllable factors, and several uncontrollable ones, that influence catch success. The main purpose of this study is to isolate and explain the influence that
the controlled factors have on catching pok\'emon.

\begin{itemize}
\item \emph{Pok\'eball type:} There are four types of pok\'eballs which are used 
in catching pok\'emon: regular pokeballs, great balls, ultra balls, and master balls.

\item \emph{Treats:} These are treats which one can give to a pok\'emon
one is attempting to catch. There are two kinds of treats we will be testing: \emph{Razz Berries} and \emph{Nanab berries.}
\end{itemize}

The following are uncontrollable factors which may influence the ability to catch pok\'emon:

\begin{itemize}
\item \emph{CP:} The Combat Power (CP) of a pok\'emon.
\item \emph{Species:} The pok\'emon species.
\item \emph{Level:} The level of the pok\'emon.
\item \emph{Level (of player):} The level of the player.
\item \emph{Quality of throw:} The quality of the pok\'eball throw will influence the catch success as well. This is a hard factor to control, so we list it here.
\end{itemize}
Hence our treatments are given by the type of pok\'eball and the kind (if any) of treat.
We have wondered whether the fact that our outcome variable is distributed
Bernoulli (catch/no catch) could be an impediment to the application of the F distribution
in our power analysis (and later hypothesis testing) because the variance of a Bernoulli depends on the
underlying probability $p$, which may differ between
the treatment groups. Given this, proceeding under the assumption of equal 
variance may not be the best course of action.


\section{Design Structure of Experiment}
Our goal is to test hypotheses about the efficacy of Pokemon hunting strategies. To this end,
the authors of this study experimented on themselves, catching Pokemon using different strategies
and recorded the resulting data. Since the data is collected by 5 different players, player
will constitute our blocking variable. Based on our power calculation we have decided that
every blocking unit (player) will collect 5 observations on the same treatment. With these
5 observations per treatment, 5 players, and 6 different treatments our experimental design
aimed at collecting 150 observations. 

\subsection{Hypotheses}
The online community has done some work on determining relative catch rates.
These hypotheses have been generated by bots that have been subsequently
forbidden by The Pokemon Company and The Pokemon Company has taken legal
action against people who continued to use such bots.
The baseline catch rate depends on the type and level of the Pokemon being
hunted. Lets denote the baseline catch rate $BCR$. Then the razz berry catch
rate is hypothesized to be the following:

\begin{equation}
RCR = 1.5 BCR
\end{equation}

The great ball catch rate is:
\begin{equation}
GBCR = 1 - (1-BCR)^{1.5}
\end{equation}

The ultra ball catch rate is supposed to be:
\begin{equation}
UBCR = 1 - (1-BCR)^{2}
\end{equation}



This information is also summarized in the following table with sample values
for $BCR$:

% latex table generated in R 3.3.1 by xtable 1.8-2 package
% Thu Dec  7 12:01:13 2017
\begin{table}[ht]
\centering
\begin{tabular}{rrrrr}
  \hline
 & bcr & gbcr & ubcr & razzcr \\ 
  \hline
1 & 0.25 & 0.35 & 0.44 & 0.38 \\ 
  2 & 0.50 & 0.65 & 0.75 & 0.75 \\ 
  3 & 0.75 & 0.88 & 0.94 & 1.12 \\ 
   \hline
\end{tabular}
\end{table}


\subsection{Power Analysis}
In an attempt to gauge how much data needs to be collected for the actual experiment,
we have done some Pokemon hunting in a pilot. This pilot did not follow an experimental
design, but was merely aiming to estimate the variance in the outcome. Some of our
members have not progressed far enough in the game to be able to use all treatments.
We have only collected data on the control group (no use of razz berry
or special pokeballs). Given that the Bernoulli variance depends on the
underlying probability $p$, which we hypothesize to be different between
the treatment groups, proceeding under the assumption of equal variance
may not be the best course of action. This is a limitation of the power
analysis that we would like to acknowledge here before proceeding with
detailed description.

\subsubsection{Parametric Power Analysis}
For our power analysis we primarily need an estimate of the variance of our outcome.
At the time of the power analysis, we have not settled on what form the outcome would take. 
And indeed, in the end we have done the analysis in such way as to be able to 
analyze the underlying questions in two different ways.
One possibility is that
a Bernoulli random variable that would represent a single ``catch''.
A catch happens when a Pokemon trainer throws pokeballs at a pokemon until this
Pokemon is ``hit'', at which point this Pokemon will be absorbed into the pokeball.
This situation may result in the Pokemon remaining in the ball ($catch = 1$)
or with the Pokemon breaking out of the ball after capture ($catch = 0$). Failure
may be followed by more hunting and eventual successful capture ($catch = 1$).

\bigskip

For the pilot power analysis data, we have collected a total of 203 observations.
In this collection, we have not randomized the razz berry use, but rather assigned
it in an ad hoc way. Our more experienced players who own more razz berries went
Pokemon hunting so in this way there would be a correlation between player level,
Pokemon level and the use of razz berry. We will detail here the process of the power
analysis as it was done at the time so as to provide an insight into the experiment
planning stage. To summarize, we have not done the pilot as a mini-experiment but
rather a collection of observational data on the outcome to gauge its variance
for the purposes of power analysis.

\bigskip

In practice, we have not collected equal samples for all treatment groups but the following
power analysis will assume that we would. We have demonstrated the ability to collect
200 samples and assuming that these would be equally distributed into four treatment 
groups (at the time of this power analysis we have not foreseen all of the six treatments
we have ended up implementing in our experimental design), our $n=50$ and we will use 
this in the following power computation. The mean 
baseline catch rate in our pilot sample is $.7$\footnote{After-the-fact note:
We have not appreciated the extent to which the baseline catch rate varies with
CP and the extent to which players in different levels encounter Pokemon with different
CP. Our pilot data contained many observations from our more experienced players
with lower baseline catch rate. The ultimate experimental design, however, attempted
balanced blocking on player.}. Based on the formulas above, this would
imply a razz catch rate of $1$ ($\tau_r = .3$), ultra ball catch rate of $.91$ ($\tau_u = .21$), and
great ball catch rate of $.84$ ($\tau_g = .14$).  The variance of the catch rate in our
pilot data is $.207$. 
Under the alternative hypothesis, the F statistic for testing the equal means
hypothesis is distributed F with $m - 1 = 3$ (4 treatment groups) and $N-m = 199$ degrees of freedom
and non-centrality parameter (assuming 50 observations per treatment group, 
which does not quite add up to 199 but that is not of the first order
of importance here).


This all leads to the following $\lambda$:

\begin{equation}
\lambda = n \frac{\sum_{i=1} \tau_i^2}{\sigma^2} = 50 \frac{(.3^2+.14^2+.21^2)}{.207} = 45.54167 
\end{equation}


Plugging these numbers into the formula for power, we obtain that a sample of
$203$ observations would have the likelihood 99.99938\% of rejecting the null
hypothesis if the alternative we specified above is true. 
This power analysis indicated to us that if we collected a sample of similar size with equal
representation of all treatment groups, we would have around 100\% chance
of rejecting the null conditional on the alternative hypothesis specified above
is true.



\subsubsection{Empirical Power Analysis -- Nonparametric Bootstrap F Test}
The previous section used distribution theory to compute power of a sample similar in 
size to the pilot sample and
operated under the assumption that variances are equal in the treatment and 
control groups. However, our outcome is distributed Bernoulli and Bernoulli variances
are functions of the underlying probability of Pokemon escaping the pokeball, so under
the alternative hypothesis these variance would be unequal. This does not affect
testing the null hypothesis, since under the null the underlying probabilities are
the same, but it does affect the power computation, because that computation involves
the alternative hypothesis. Consequently, in this section we will perform a Monte Carlo 
exercise to determine power empirically and compare the results with our result above.
In light of what we described in the section above about different pokemon having
different baseline catch rate, we could actually influence our baseline catch rate
by ignoring some pokemon and focusing on others. Since the variance of the Bernoulli
is a function of the baseline catch rate, we can manipulate variance this way and
this affects power. Hence, in the following Monte Carlo simulation, we have run
the simulations with different baseline catch rates to capture not only the relationship
between sample size and power, but also between baseline catch rate and power.




% latex table generated in R 3.3.1 by xtable 1.8-2 package
% Thu Dec  7 12:01:17 2017
\begin{table}[ht]
\centering
\begin{tabular}{rlrr}
  \hline
 & bcr & sample & power \\ 
  \hline
1 & 0.25 & 25.00 & 0.60 \\ 
  2 & 0.5 & 25.00 & 0.80 \\ 
  3 & 0.75 & 25.00 & 1.00 \\ 
  4 & 0.25 & 50.00 & 1.00 \\ 
  5 & 0.5 & 50.00 & 1.00 \\ 
  6 & 0.75 & 50.00 & 1.00 \\ 
  7 & 0.25 & 75.00 & 1.00 \\ 
  8 & 0.5 & 75.00 & 1.00 \\ 
  9 & 0.75 & 75.00 & 1.00 \\ 
  10 & 0.25 & 100.00 & 1.00 \\ 
  11 & 0.5 & 100.00 & 1.00 \\ 
  12 & 0.75 & 100.00 & 1.00 \\ 
  13 & 0.25 & 125.00 & 1.00 \\ 
  14 & 0.5 & 125.00 & 1.00 \\ 
  15 & 0.75 & 125.00 & 1.00 \\ 
   \hline
\end{tabular}
\caption{Power for various baseline catch rates and sample sizes} 
\end{table}


\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}

{\centering \includegraphics[width=\maxwidth]{figure/minimal-power-graph-1} 

}

\caption[Power, sample size and outcome variance (BCR)]{Power, sample size and outcome variance (BCR)}\label{fig:power-graph}
\end{figure}


\end{knitrout}

\clearpage

\subsection{Defining the Outcome}
One major issue to be considered in the experimental design was the outcome. At the most granular
level the outcome is binary -- catch or no catch, or equivalently Pokemon escape or Pokemon does
not escape. However, there is alternatively conceptualization of the issue -- how many times
Pokemon escapes the ball before it is caught. This latter conceptualization has a weakness
-- sometimes Pokemon is not caught at all, but rather flees after several unsuccessful
attempts at its capture. Hence, a refinement of this aggregate outcome measure would
be the outcome define as how many times Pokemon escapes containment (ball), before it
either flees or is captured. We have decided to collect the data at the most granular
level (binary outcome; catch or no catch), while at the same time randomize at the more
aggregate level of a Pokemon ``encounter.'' ``Pokemon encounter'' would be here defined
as a situation where Pokemon trainer (player/experimenter) runs into a Pokemon and starts
a sequence of catch attempts. The individual catch attempts with the same Pokemon are binary,
however, the aggregate outcome of how many times this same Pokemon escapes from the ball
is an outcome at the ``Pokemon encounter'' level. Since we wanted to be able to analyze
the experiment from both perspectives, we have collected data at the attempt level, however,
randomization was done at the encounter level. Same treatment was applied repeatedly if
Pokemon escaped the first attempt at containment and this treatment was applied for so long
as this same Pokemon kept escaping from containment.


\subsection{Potential Issues}
One potential issue stemming from our experimental design is that Pokemon who are harder to
catch will be oversampled in our analyses that use the binary outcome. An issue with defining
the outcome as ``how many attempts before Pokemon flees or is captured'' is that it's less
interesting than ``how many attempts before Pokemon is captured''. However, the latter
definition would cause us trouble, since a fleeing Pokemon would have to be coded as
infinity.


\subsection{Treatment Assignment}
With all that said, we have enumerated all the treatments that need to be done (all treatment-player
combinations repeated 5 times) and define the variable ``time'' to conceptualize the fact
that ours is a dynamic experiment, in which a players encounters a sequence of Pokemon,
so while some agricultural experiments have a spatial dimension to their experimental 
design, in our case this dimension is a temporal one. ``Time'' is then a discrete variable
that enumerates Pokemon encounters from 1 to 30. We have constructed a dataset containing
three variables: player, treatment, time. In this dataset, we have reshuffled treatments
within players using the R functions \verb|sample()| and \verb|tapply()|. The resulting 
treatment assignment is shown in the five tables in this section (one for each player).

% latex table generated in R 3.3.1 by xtable 1.8-2 package
% Thu Dec  7 12:01:17 2017
\begin{table}[ht]
\centering
\begin{tabular}{rlllll}
  \hline
 & Wenbo & Asghar & Clayton & Huixiang & Nail \\ 
  \hline
1 & Razz+Poke & Razz+Great & Ultra & Razz+Great & Razz+Ultra \\ 
  2 & Razz+Great & Razz+Poke & Razz+Poke & Razz+Great & Razz+Poke \\ 
  3 & Poke & Ultra & Great & Razz+Ultra & Ultra \\ 
  4 & Razz+Ultra & Razz+Poke & Poke & Razz+Great & Poke \\ 
  5 & Ultra & Razz+Ultra & Great & Great & Razz+Ultra \\ 
  6 & Poke & Razz+Great & Razz+Ultra & Razz+Ultra & Great \\ 
  7 & Razz+Ultra & Razz+Great & Razz+Ultra & Ultra & Poke \\ 
  8 & Razz+Ultra & Great & Razz+Poke & Poke & Razz+Poke \\ 
  9 & Razz+Great & Ultra & Razz+Great & Ultra & Razz+Ultra \\ 
  10 & Great & Great & Ultra & Razz+Ultra & Razz+Great \\ 
  11 & Razz+Poke & Razz+Poke & Razz+Poke & Ultra & Poke \\ 
  12 & Ultra & Poke & Poke & Razz+Poke & Razz+Great \\ 
  13 & Great & Razz+Great & Poke & Great & Great \\ 
  14 & Razz+Poke & Razz+Ultra & Great & Razz+Ultra & Razz+Ultra \\ 
  15 & Great & Razz+Ultra & Razz+Ultra & Ultra & Ultra \\ 
  16 & Ultra & Poke & Razz+Great & Poke & Poke \\ 
  17 & Razz+Great & Poke & Great & Razz+Poke & Razz+Poke \\ 
  18 & Poke & Poke & Razz+Great & Razz+Great & Razz+Ultra \\ 
  19 & Razz+Poke & Razz+Great & Poke & Razz+Poke & Poke \\ 
  20 & Great & Ultra & Razz+Poke & Razz+Ultra & Great \\ 
  21 & Razz+Great & Razz+Poke & Ultra & Ultra & Great \\ 
  22 & Poke & Razz+Ultra & Ultra & Poke & Ultra \\ 
  23 & Great & Ultra & Razz+Ultra & Poke & Razz+Poke \\ 
  24 & Razz+Poke & Razz+Ultra & Ultra & Razz+Poke & Ultra \\ 
  25 & Razz+Ultra & Great & Razz+Great & Great & Razz+Great \\ 
  26 & Ultra & Razz+Poke & Poke & Great & Razz+Poke \\ 
  27 & Ultra & Great & Razz+Poke & Poke & Great \\ 
  28 & Poke & Poke & Razz+Ultra & Great & Ultra \\ 
  29 & Razz+Ultra & Great & Razz+Great & Razz+Poke & Razz+Great \\ 
  30 & Razz+Great & Ultra & Great & Razz+Great & Razz+Great \\ 
   \hline
\end{tabular}
\caption{Treatment assignment to blocking units} 
\end{table}



\clearpage

\section{Data Analysis}
\subsection{Naive ANOVA}
We have wondered whether the fact that our outcome variable is distributed
Bernoulli could be an impediment to the application of ANOVA.
The central feature of ANOVA is the application of 
the F distribution to the ratio of treatment sum of squares and
residual sum of squares. These are distributed $\chi^2$ if the
underlying summands are normally distributed: 

\begin{align}
F &= \frac{MST}{MSE} \\
    &= \frac{\frac{SST}{m-1}}{\frac{SSE}{m(n-1)}} \\
    &= \frac{\frac{SST}{m-1}}{\frac{\sum_{i=1}^m \sum_{j=1}^n (y_{ij} - \bar{y}_i)^2 }{m(n-1)}} 
\end{align}

The above equation shows for example that if $(y_{ij} - \bar{y}_i)$ is normal, then
$(y_{ij} - \bar{y}_i)^2$ is $\chi_1^2$ and $\sum_{i=1}^m \sum_{j=1}^n (y_{ij} - \bar{y}_i)^2$
is  $\chi_{m(n-1)}^2$. The key issue here is that $y_{ij}$ is not Normal but
Bernoulli. What does that tell us about the distribution of  $(y_{ij} - \bar{y}_i)$?
What is the distribution of the difference between a Bernoulli and a Normal?
The following simulation suggests that this is normally distributed.

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{hist}\hlstd{(}\hlkwd{rbinom}\hlstd{(}\hlnum{1000}\hlstd{,} \hlnum{1}\hlstd{,} \hlnum{.5}\hlstd{)}\hlopt{-}\hlkwd{rnorm}\hlstd{(}\hlnum{1000}\hlstd{))}
\end{alltt}
\end{kframe}

{\centering \includegraphics[width=\maxwidth]{figure/minimal-unnamed-chunk-7-1} 

}



\end{knitrout}

\bigskip

Similarly with the numerator:
\begin{align}
F &= \frac{MST}{MSE} \\
    &= \frac{\frac{SST}{m-1}}{\frac{SSE}{m(n-1)}} \\
    &= \frac{\frac{\sum_{i=1}^m \sum_{j=1}^n (\bar{y}_{i.} - \bar{y}_{..})^2}{m-1}}{\frac{SSE}{m(n-1)}} 
\end{align}

For the SST to be $\chi_{(m-1)}^2$, we need both $\bar{y}_{i.}$ and - $\bar{y}_{..}$ to 
be normally distributed, but
with sufficiently high $m$ and $n$, this should not be a problem even if $y_{ij}$
is distributed Bernoulli as the Central Limit Theorem suggests.

The following simulation illustrates the Central Limit theorem:

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{catch_with_razz} \hlkwb{<-} \hlkwd{rbinom}\hlstd{(}\hlnum{500}\hlstd{,} \hlnum{1}\hlstd{,} \hlnum{.9}\hlstd{)}
\hlstd{catch_wo_razz} \hlkwb{<-} \hlkwd{rbinom}\hlstd{(}\hlnum{500}\hlstd{,} \hlnum{1}\hlstd{,} \hlnum{.6}\hlstd{)}
\hlstd{catch} \hlkwb{<-} \hlkwd{c}\hlstd{(catch_with_razz, catch_wo_razz)}
\hlstd{data} \hlkwb{<-} \hlkwd{data.frame}\hlstd{(}\hlkwc{catch} \hlstd{= catch)}
\end{alltt}
\end{kframe}
\end{knitrout}


\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}

{\centering \includegraphics[width=\maxwidth]{figure/minimal-bernoulli-1} 

}

\caption[Does not look normall and is indeed Bernoulli]{Does not look normall and is indeed Bernoulli}\label{fig:bernoulli}
\end{figure}


\end{knitrout}



\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}

{\centering \includegraphics[width=\maxwidth]{figure/minimal-many-bernoulli-1} 

}

\caption[Sample average of a Bernoulli on the other hand looks fairly normal (CLT)]{Sample average of a Bernoulli on the other hand looks fairly normal (CLT)}\label{fig:many-bernoulli}
\end{figure}


\end{knitrout}

This exercise confirms the conclusion of the Central Limit theorem than the mean of many Bernoulli
outcomes is normally distributed. In our analysis, we will investigate our questions both
using ANOVA under the assumption that relevant quantities are normally distributed as well
as using other models and compare whether the Central Limit Theorem is applicable in
our situation. The following provides the ANOVA estimation based on the above discussion
suggesting that with large enough sample the assumption of the F statistic distributed
F with the relevant degrees of freedom might not be that far off:

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{data} \hlkwb{<-} \hlkwd{read.csv}\hlstd{(}\hlstr{"poke_aggregate_data.csv"}\hlstd{)}
\hlstd{lmmod} \hlkwb{<-} \hlkwd{lm}\hlstd{(catch_num}\hlopt{~}
    \hlkwd{as.factor}\hlstd{(player)} \hlopt{+}
    \hlkwd{as.factor}\hlstd{(message)} \hlopt{+}
    \hlstd{cp} \hlopt{+}
    \hlkwd{as.factor}\hlstd{(razzberry)} \hlopt{+}
    \hlkwd{as.factor}\hlstd{(ball),}
    \hlkwc{data} \hlstd{= data}
\hlstd{)}
\end{alltt}
\end{kframe}
\end{knitrout}

% latex table generated in R 3.3.1 by xtable 1.8-2 package
% Thu Dec  7 12:01:18 2017
\begin{table}[ht]
\centering
\begin{tabular}{lrrrrr}
  \hline
 & Df & Sum Sq & Mean Sq & F value & Pr($>$F) \\ 
  \hline
as.factor(player) & 4 & 1.47 & 0.37 & 2.07 & 0.0870 \\ 
  as.factor(message) & 2 & 1.21 & 0.61 & 3.40 & 0.0357 \\ 
  cp & 1 & 1.43 & 1.43 & 8.02 & 0.0052 \\ 
  as.factor(razzberry) & 1 & 0.68 & 0.68 & 3.84 & 0.0516 \\ 
  as.factor(ball) & 2 & 0.25 & 0.13 & 0.72 & 0.4904 \\ 
  Residuals & 165 & 29.39 & 0.18 &  &  \\ 
   \hline
\end{tabular}
\caption{Naive ANOVA model} 
\end{table}


Our naive ANOVA model suggests that the razzberry treatment is significant, however, the
ball seems to make no difference to catch probability. It also suggests the importance
of different throw quality and Pokemon Combat Power. We will see these conclusions confirmed
with a logistic regression as well as Poisson GLM model in the sections to follow. 
All in all, the CLT, our Monte Carlo simulation and the logistic regression/Poisson
GLM results suggest that the application of naive ANOVA to our data is not entirely 
inappropriate.


\subsection{Logistic Regression}
In this section, we proceed with the analysis using a binary outcome with data at the attempt level.
To do this, we use the logistic regression, which generates predictions bound by 0 and 1 as is 
appropriate for an outcome that is a probability of catching a Pokemon. The results from the
logistic regression indicate that only the Razzberry treatment has significant impact on the
probability of catch a Pokemon, while the ball type does not appear to be significant.
This is in line with our results from the Poisson regression on the aggregated outcome
as we will see in the following section. The only other variable that has significant
impact is the Pokemon's CP (Combat Power), however, this variable is merely a co-variate
and was not experimentally manipulated. This variable can never be under our perfect control,
which is why we refrained from including it in our experimental design. ANOVA results
from sequential sum of squares analysis confirm the results stated above. 


\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\maxwidth]{figure/minimal-unnamed-chunk-12-1} 
\includegraphics[width=\maxwidth]{figure/minimal-unnamed-chunk-12-2} 
\includegraphics[width=\maxwidth]{figure/minimal-unnamed-chunk-12-3} 
\includegraphics[width=\maxwidth]{figure/minimal-unnamed-chunk-12-4} 

}



\end{knitrout}


% Table created by stargazer v.5.2 by Marek Hlavac, Harvard University. E-mail: hlavac at fas.harvard.edu
% Date and time: Thu, Dec 07, 2017 - 12:01:18 PM
\begin{table}[!htbp] \centering 
  \caption{Summary Statistics for the Data} 
  \label{} 
\begin{tabular}{@{\extracolsep{5pt}}lccccc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
Statistic & \multicolumn{1}{c}{N} & \multicolumn{1}{c}{Mean} & \multicolumn{1}{c}{St. Dev.} & \multicolumn{1}{c}{Min} & \multicolumn{1}{c}{Max} \\ 
\hline \\[-1.8ex] 
cp & 176 & 251.494 & 203.893 & 10 & 903 \\ 
razzberry & 176 & 0.472 & 0.501 & 0 & 1 \\ 
catch\_num & 176 & 0.733 & 0.444 & 0 & 1 \\ 
\hline \\[-1.8ex] 
\end{tabular} 
\end{table} 



% Table created by stargazer v.5.2 by Marek Hlavac, Harvard University. E-mail: hlavac at fas.harvard.edu
% Date and time: Thu, Dec 07, 2017 - 12:01:18 PM
\begin{table}[!htbp] \centering 
  \caption{Logistic Regression Results} 
  \label{} 
\begin{tabular}{@{\extracolsep{5pt}}lc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
 & \multicolumn{1}{c}{\textit{Dependent variable:}} \\ 
\cline{2-2} 
\\[-1.8ex] & catch\_num \\ 
\hline \\[-1.8ex] 
 Clayton & 0.194 \\ 
  & (1.213) \\ 
  & \\ 
 Huixiang & $-$0.142 \\ 
  & (0.886) \\ 
  & \\ 
 Nail & $-$0.244 \\ 
  & (0.884) \\ 
  & \\ 
 Wenbo & 0.062 \\ 
  & (0.839) \\ 
  & \\ 
 CP & $-$0.007$^{***}$ \\ 
  & (0.002) \\ 
  & \\ 
 Nice & $-$1.061 \\ 
  & (0.946) \\ 
  & \\ 
 No Message & $-$2.010$^{*}$ \\ 
  & (1.046) \\ 
  & \\ 
 Poke Ball & 0.063 \\ 
  & (0.652) \\ 
  & \\ 
 Ultra Ball & 0.137 \\ 
  & (0.675) \\ 
  & \\ 
 Razzberry & 1.143$^{**}$ \\ 
  & (0.577) \\ 
  & \\ 
 Constant & 3.742$^{*}$ \\ 
  & (2.261) \\ 
  & \\ 
\hline \\[-1.8ex] 
Observations & 176 \\ 
Log Likelihood & $-$62.034 \\ 
Akaike Inf. Crit. & 218.069 \\ 
\hline 
\hline \\[-1.8ex] 
\textit{Note:}  & \multicolumn{1}{r}{$^{*}$p$<$0.1; $^{**}$p$<$0.05; $^{***}$p$<$0.01} \\ 
\end{tabular} 
\end{table} 


% latex table generated in R 3.3.1 by xtable 1.8-2 package
% Thu Dec  7 12:01:18 2017
\begin{table}[ht]
\centering
\begin{tabular}{lrrrrr}
  \hline
 & Df & Deviance & Resid. Df & Resid. Dev & Pr($>$Chi) \\ 
  \hline
NULL &  &  & 175 & 204.26 &  \\ 
  as.factor(player) & 4 & 8.09 & 171 & 196.18 & 0.0883 \\ 
  as.factor(pokemon) & 36 & 48.28 & 135 & 147.90 & 0.0829 \\ 
  cp & 1 & 15.26 & 134 & 132.64 & 0.0001 \\ 
  as.factor(message) & 2 & 4.27 & 132 & 128.36 & 0.1180 \\ 
  as.factor(ball) & 2 & 0.07 & 130 & 128.29 & 0.9641 \\ 
  as.factor(razzberry) & 1 & 4.22 & 129 & 124.07 & 0.0400 \\ 
   \hline
\end{tabular}
\caption{ANOVA applied to the first Logistic Regression} 
\end{table}



\clearpage

\subsection{GLM Poisson}
At this point (given our earlier non-findings on the ball type effect), we were concerned that 
perhaps we have not collected enough data, so we have sent one of our blocking unit (experimenter)
to collect more data. This resulted in addition of new data, however, the original balance
(quasi-balance given that two blocking units did not have access to the Ultra Ball treatment)
was lost. At the same time, our earlier analysis has shown that controlling for CP
and throw quality, the identity of the player (blocking unit) does not seem to have
much bearing on the outcome. Table~\ref{tbl:ballesc}, Table~\ref{tbl:razzesc}, and Table~\ref{tbl:playeresc}
describe this dataset. The following paragraph describes the GLM Poisson analysis of this extended
dataset.

\bigskip

In this section, we present results using Generalized Linear Model with log as the link
function and outcome assumed distributed Poisson. The outcome is actually more accurately
described as being geometrically distributed, so Poisson is an approximation in this approach.
We aggregate our original granular data by first defining an ``escape'' variable, which is
define as $escape = 1 - catch$, and summing over all observations within a single Pokemon 
encounter as explain in the experimental design section. Since in our experimental design,
we intentionally replicated the same treatment within the same Pokemon encounter, 
this aggregation goes through with few obstacles -- only throw message cannot be aggregated
and is not used in the analysis (as opposed to the analysis at the attempt level).
In Table~\ref{tbl:poi} we present two Poisson models, one with controls for
player and the other without. The reference group (``Constant'') represents
number of escapes from player Asghar using Great Ball without a Razzberry.
This analysis shows Razzberry as being a significant factor in Pokemon hunting, however,
the evidence on ball type does not suggest it is very important. CP is only
a co-variate in our analysis, rather than an experimental treatment, however,
it also emerges as a significant factor in Pokemon hunting.






% Table created by stargazer v.5.2 by Marek Hlavac, Harvard University. E-mail: hlavac at fas.harvard.edu
% Date and time: Thu, Dec 07, 2017 - 12:01:19 PM
\begin{table}[!htbp] \centering 
  \caption{GLM Model Using Poisson Distribution} 
  \label{tbl:poi} 
\begin{tabular}{@{\extracolsep{5pt}}lcc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
 & \multicolumn{2}{c}{\textit{Dependent variable:}} \\ 
\cline{2-3} 
\\[-1.8ex] & \multicolumn{2}{c}{Number of Pokemon Escapes} \\ 
\\[-1.8ex] & (1) & (2)\\ 
\hline \\[-1.8ex] 
 Poke Ball &  & $-$1.083 \\ 
  &  & (0.803) \\ 
  & & \\ 
 Ultra Ball &  & $-$0.013 \\ 
  &  & (0.512) \\ 
  & & \\ 
 Razzberry &  & $-$0.303 \\ 
  &  & (0.542) \\ 
  & & \\ 
 CP &  & $-$0.229 \\ 
  &  & (0.522) \\ 
  & & \\ 
 Clayton & 0.478 & 0.504 \\ 
  & (0.382) & (0.386) \\ 
  & & \\ 
 Huixiang & 0.851$^{**}$ & 0.801$^{**}$ \\ 
  & (0.367) & (0.392) \\ 
  & & \\ 
 Nail & $-$0.757$^{**}$ & $-$0.755$^{**}$ \\ 
  & (0.312) & (0.314) \\ 
  & & \\ 
 Wenbo & 0.003$^{***}$ & 0.002$^{***}$ \\ 
  & (0.001) & (0.001) \\ 
  & & \\ 
 Constant & $-$1.853$^{***}$ & $-$1.589$^{***}$ \\ 
  & (0.363) & (0.459) \\ 
  & & \\ 
\hline \\[-1.8ex] 
Observations & 136 & 136 \\ 
Log Likelihood & $-$100.923 & $-$99.480 \\ 
Akaike Inf. Crit. & 211.846 & 216.961 \\ 
\hline 
\hline \\[-1.8ex] 
\textit{Note:}  & \multicolumn{2}{r}{$^{*}$p$<$0.1; $^{**}$p$<$0.05; $^{***}$p$<$0.01} \\ 
\end{tabular} 
\end{table} 


%<<echo=FALSE,results='asis'>>=
%stargazer(data, label = "tbl:exdata", title = "Summary Statistics for Extended Data Aggregated at the Pokemon Encounter Level")
%@

\begin{table}[!h] \centering
\caption{Escapes by Ball Type}\label{tbl:ballesc}
\begin{tabular}{lccc}
\hline
 &  & \multicolumn{2}{c}{escapes} \\ 
Ball Type  & n & Mean & \multicolumn{1}{c}{Sd} \\ 
\hline
Great Ball  & $\phantom{0}55$ & $0.25$ & $0.75$ \\
Poke Ball  & $\phantom{0}51$ & $0.31$ & $0.68$ \\
Ultra Ball  & $\phantom{0}30$ & $0.57$ & $0.97$ \\
All  & $136$ & $0.35$ & $0.78$ \\
\hline 
\end{tabular}
\end{table}
\begin{table}[!h] \centering
\caption{Escapes by Razzberry Use}\label{tbl:razzesc}
\begin{tabular}{lccc}
\hline
 &  & \multicolumn{2}{c}{escapes} \\ 
Razzberry Used  & n & Mean & \multicolumn{1}{c}{Sd} \\ 
\hline
0  & $\phantom{0}67$ & $0.45$ & $0.89$ \\
1  & $\phantom{0}69$ & $0.25$ & $0.65$ \\
All  & $136$ & $0.35$ & $0.78$ \\
\hline 
\end{tabular}
\end{table}
\begin{table}[!h] \centering
\caption{Escapes by Player}\label{tbl:playeresc}
\begin{tabular}{lccc}
\hline
 &  & \multicolumn{2}{c}{escapes} \\ 
Hunting Player  & n & Mean & \multicolumn{1}{c}{Sd} \\ 
\hline
Asghar  & $\phantom{0}26$ & $0.27$ & $0.87$ \\
Clayton  & $\phantom{0}20$ & $0.10$ & $0.31$ \\
Huixiang  & $\phantom{0}30$ & $0.53$ & $0.90$ \\
Nail  & $\phantom{0}30$ & $0.27$ & $0.58$ \\
Wenbo  & $\phantom{0}30$ & $0.47$ & $0.94$ \\
All  & $136$ & $0.35$ & $0.78$ \\
\hline 
\end{tabular}
\end{table}








\end{document}
