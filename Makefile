pokemon: pokemon.tex
	pdflatex pokemon.tex

pokemon.tex: pokemon.Rnw
	Rscript -e "require(knitr); knit('pokemon.Rnw')"

clean:
	rm -f *.bbl *.blg *.txt *.aux *.log *.out *.Rout *.glg *.glo *.gls *.ist 
	rm -f *.toc *.nav *.snm *.vrb *.4ct *.4tc *.bcf *.xml *.xref *.zip *.tmp
	rm -fr figure/
